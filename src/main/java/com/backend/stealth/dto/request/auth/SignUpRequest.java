package com.backend.stealth.dto.request.auth;

import lombok.Getter;
import lombok.Setter;

import java.util.UUID;

@Setter
@Getter
public class SignUpRequest {
    private String email;
    private String thumbnail;
    private String username;
    private String password;


}
