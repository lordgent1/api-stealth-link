package com.backend.stealth.dto.request.auth;

import lombok.Getter;
import lombok.Setter;

@Setter
@Getter
public class SignInRequest {
    private String username;
    private String password;
}
