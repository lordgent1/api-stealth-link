package com.backend.stealth.dto.request.messages;

import lombok.Getter;
import lombok.Setter;

import java.util.UUID;

@Setter
@Getter
public class CreateQaRequest {
    private String id;
    private String title;

    private CreateQaRequest(){
        id = UUID.randomUUID().toString();
    }
}
