package com.backend.stealth.dto.request.messages;

import lombok.Getter;
import lombok.Setter;

@Setter
@Getter
public class QandARequest {
    private String username;
    private String id;
}
