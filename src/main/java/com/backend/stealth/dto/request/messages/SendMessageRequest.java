package com.backend.stealth.dto.request.messages;

import lombok.Getter;
import lombok.Setter;

@Setter
@Getter
public class SendMessageRequest {
    private String message;
    private String id;
}
