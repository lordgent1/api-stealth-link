package com.backend.stealth.dto.request.users;

import lombok.Getter;
import lombok.Setter;

@Setter
@Getter
public class ShowProfileRequest {
    private String username;
}
