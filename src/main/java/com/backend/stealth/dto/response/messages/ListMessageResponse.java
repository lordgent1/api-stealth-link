package com.backend.stealth.dto.response.messages;

import lombok.Getter;
import lombok.Setter;

@Setter
@Getter
public class ListMessageResponse {
    private String recipient;
    private String messages;
}
