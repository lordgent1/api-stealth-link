package com.backend.stealth.dto.response.users;

import lombok.Getter;
import lombok.Setter;

@Setter
@Getter
public class ShowProfileResponse {
    private String username;
    private String email;
    private String avatar;
    private int totalSender;
}
