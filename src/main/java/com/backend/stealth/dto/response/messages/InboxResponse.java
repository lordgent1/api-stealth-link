package com.backend.stealth.dto.response.messages;

import lombok.Getter;
import lombok.Setter;

@Setter
@Getter
public class InboxResponse {
    private String id;
    private String username;
    private String title;
    private long totalSender;
}
