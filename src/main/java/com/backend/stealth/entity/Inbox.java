package com.backend.stealth.entity;

import jakarta.persistence.*;
import lombok.Getter;
import lombok.Setter;

@Entity
@Setter
@Getter
public class Inbox extends BaseEntity {
    @Id
    private String id;
    @Column(name = "title")
    private String title;
    @ManyToOne
    @JoinColumn(columnDefinition = "user_id")
    private Users user;
}
