package com.backend.stealth.entity;

import jakarta.persistence.*;
import lombok.Getter;
import lombok.Setter;


@Setter
@Getter
@Entity
public class Message extends BaseEntity {
    @Id
    private String id;
    @Column(name = "description")
    private String description;
    @ManyToOne
    @JoinColumn(columnDefinition = "inbox_id")
    private Inbox inbox;
}
