package com.backend.stealth.controller;

import com.backend.stealth.dto.request.auth.SignUpRequest;
import com.backend.stealth.service.users.UserService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.*;

@RestController
@Controller
@RequestMapping("/app/api/v1/user")
public class UsersController {

    @Autowired
    UserService userService;

    @GetMapping("/profile")
    public ResponseEntity<Object> getProfile(){
        return userService.getProfile();
    }

}
