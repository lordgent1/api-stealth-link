package com.backend.stealth.controller;

import com.backend.stealth.dto.request.auth.SignInRequest;
import com.backend.stealth.dto.request.auth.SignUpRequest;
import com.backend.stealth.service.auth.AuthService;
import io.swagger.v3.oas.annotations.Operation;
import io.swagger.v3.oas.annotations.responses.ApiResponse;
import io.swagger.v3.oas.annotations.responses.ApiResponses;
import io.swagger.v3.oas.annotations.tags.Tag;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@Tag(name = "Authentication", description = "API endpoints for user login and registration.")
@RestController
@Controller
@RequestMapping("/app/api/v1/auth")
public class AuthController {
    @Autowired
    private AuthService authService;


    @Operation(
            summary = "User Sign Up",
            description = "Registers a new user with the provided information."
    )
    @ApiResponses(value = {
            @ApiResponse(responseCode = "200", description = "User registration successful"),
            @ApiResponse(responseCode = "400", description = "Bad request, invalid input"),
    })
    @PostMapping("/signup")
    public ResponseEntity<Object> signUp(@RequestBody SignUpRequest request){
        return authService.signUp(request);
    }

    @Operation(
            summary = "User Sign In",
            description = "Authenticates and logs in a user based on the provided credentials."
    )
    @ApiResponses(value = {
            @ApiResponse(responseCode = "200", description = "User authentication successful"),
            @ApiResponse(responseCode = "400", description = "Bad request, invalid input"),
            @ApiResponse(responseCode = "401", description = "Unauthorized, invalid credentials"),
    })
    @PostMapping("/signin")
    public ResponseEntity<Object> signIn(@RequestBody SignInRequest request){
        return authService.signIn(request);
    }


}
