package com.backend.stealth.controller;


import com.backend.stealth.dto.request.messages.CreateQaRequest;
import com.backend.stealth.service.messages.MessageService;
import io.swagger.v3.oas.annotations.Operation;
import io.swagger.v3.oas.annotations.responses.ApiResponse;
import io.swagger.v3.oas.annotations.responses.ApiResponses;
import io.swagger.v3.oas.annotations.tags.Tag;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.*;

@Tag(
        name = "Management Q&A",
        description =
        "API endpoints for users to create Q&A, " +
        "Display a list of created Q&A, and see how many people have answered the Q&A")
@RestController
@Controller
@RequestMapping("/app/api/v1/user")
public class MessageController {

    @Autowired
    MessageService messageService;

    @Operation(
            summary = "Create Q&A by Creator",
            description = "API for allowing creators to create Q&A."
    )
    @ApiResponses(value = {
            @ApiResponse(responseCode = "200", description = "Q&A creation successful"),
    })
    @PostMapping("/inbox")
    public ResponseEntity<Object> createInbox(@RequestBody CreateQaRequest request){
        return  messageService.createTitleQAndDA(request);
    }

    @Operation(
            summary = "List Q&A by Creator",
            description = "API for retrieving a list of Q&A created by the user."
    )
    @ApiResponses(value = {
            @ApiResponse(responseCode = "200", description = "List of Q&A retrieval successful"),
    })
    @GetMapping("/inbox")
    public ResponseEntity<Object> listInboxUSER(){
        return  messageService.getInboxUser();
    }

}

