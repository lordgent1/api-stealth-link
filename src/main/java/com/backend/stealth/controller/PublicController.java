package com.backend.stealth.controller;

import com.backend.stealth.dto.request.messages.SendMessageRequest;
import com.backend.stealth.service.messages.MessageService;
import io.swagger.v3.oas.annotations.Operation;
import io.swagger.v3.oas.annotations.responses.ApiResponse;
import io.swagger.v3.oas.annotations.responses.ApiResponses;
import io.swagger.v3.oas.annotations.tags.Tag;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.*;

@Tag(name = "Public Q&A", description = "API endpoints for users to access links to Q&A and submit answers. No authentication required.")
@RestController
@Controller
@RequestMapping("/app/api/v1/public")
public class PublicController {

    @Autowired
    MessageService messageService;

    @Operation(
            summary = "Show Q&A via Link Sharing",
            description = "API for retrieving Q&A obtained through a provided link."
    )
    @ApiResponses(value = {
            @ApiResponse(responseCode = "200", description = "successful"),
    })
    @GetMapping("/hi")
    public ResponseEntity<Object> findQAndA(@RequestParam("id") String id,@RequestParam("username") String username) {
        return messageService.showUserQuestion(id, username);
    }

    @Operation(
            summary = "Send Message to Q&A Creator",
            description = "API for sending a message to user"
    )
    @ApiResponses(value = {
            @ApiResponse(responseCode = "200", description = "Message sent successfully"),
    })
    @PostMapping("/inbox")
    public ResponseEntity<Object> sendMessage(@RequestBody SendMessageRequest request) {
        return messageService.sendMessage(request);
    }

}

