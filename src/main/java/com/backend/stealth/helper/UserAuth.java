package com.backend.stealth.helper;

import com.backend.stealth.entity.Users;
import com.backend.stealth.repository.UsersRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.stereotype.Component;

import java.util.Objects;

@Component
public class UserAuth {

    @Autowired
    private UsersRepository userRepository;
    public Users getUserLogin(){
        var auth = (Authentication) SecurityContextHolder.getContext().getAuthentication();
        var name = auth.getName();
        var isAuth = userRepository.findByUsername(name).orElseThrow();
        return Objects.requireNonNullElseGet(isAuth, () -> {
            return isAuth;
        });
    }
}
