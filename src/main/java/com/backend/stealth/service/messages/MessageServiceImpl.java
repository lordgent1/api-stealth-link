package com.backend.stealth.service.messages;

import com.backend.stealth.dto.request.messages.CreateQaRequest;
import com.backend.stealth.dto.request.messages.QandARequest;
import com.backend.stealth.dto.request.messages.SendMessageRequest;
import com.backend.stealth.dto.response.ResponseHandler;
import com.backend.stealth.dto.response.messages.InboxResponse;
import com.backend.stealth.dto.response.messages.QAndAResponse;
import com.backend.stealth.dto.response.users.ShowProfileResponse;
import com.backend.stealth.entity.Inbox;
import com.backend.stealth.entity.Message;
import com.backend.stealth.helper.UserAuth;
import com.backend.stealth.repository.InboxRepository;
import com.backend.stealth.repository.MessageRepository;
import org.modelmapper.ModelMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.UUID;

@Service
public class MessageServiceImpl implements MessageService {

    @Autowired
    InboxRepository inboxRepository;
    @Autowired
    UserAuth userAuth;
    @Autowired
    MessageRepository messageRepository;

    @Override
    public ResponseEntity<Object> sendMessage(SendMessageRequest request) {
        ModelMapper modelMapper = new ModelMapper();
        Message data = modelMapper.map(request, Message.class);

        Inbox findInbox = inboxRepository.findById(request.getId())
                .orElseThrow();

        data.setId(UUID.randomUUID().toString());
        data.setInbox(findInbox);

        Message save = messageRepository.save(data);
        String inboxTitle = modelMapper.map(save.getInbox(), String.class);

        return ResponseHandler.generateResponseSuccess(inboxTitle);
    }

    @Override
    public ResponseEntity<Object> getInboxUser() {
        List<Object[]> inboxData = inboxRepository.listInboxUser(userAuth.getUserLogin().getUsername());

        List<InboxResponse> data = inboxData.stream()
                .map(objects -> {
                    InboxResponse inboxResponse = new InboxResponse();
                    inboxResponse.setId((String) objects[0]);
                    inboxResponse.setTitle((String) objects[1]);
                    inboxResponse.setUsername((String) objects[2]);
                    inboxResponse.setTotalSender(((long) objects[3]));
                    return inboxResponse;
                })
                .toList();

        return ResponseHandler.generateResponseSuccess(data);
    }

    @Override
    public ResponseEntity<Object> createTitleQAndDA(CreateQaRequest request) {

        ModelMapper modelMapper = new ModelMapper();
        Inbox req = modelMapper.map(request,Inbox.class);
        req.setUser(userAuth.getUserLogin());

        Inbox inbox = inboxRepository.save(req);
        return ResponseHandler.generateResponseSuccess(inbox.getTitle());

    }

    @Override
    public ResponseEntity<Object> showUserQuestion(String id,String username) {
        List<Object[]> find = inboxRepository.findQAndA(id, username);

        if (find.isEmpty()) {
            return ResponseHandler.generateResponseError(HttpStatus.NOT_FOUND, "Not Found", "Not Found Q&A");
        }

        QAndAResponse response = find.stream()
                .map(objects -> {
                    QAndAResponse qAndAResponse = new QAndAResponse();
                    qAndAResponse.setId((String) objects[0]);
                    qAndAResponse.setUsername((String) objects[1]);
                    qAndAResponse.setTitle((String) objects[2]);
                    return qAndAResponse;
                })
                .findFirst()
                .orElseThrow();

        return ResponseHandler.generateResponseSuccess(response);

    }

}
