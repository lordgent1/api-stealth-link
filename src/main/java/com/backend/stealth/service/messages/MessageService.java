package com.backend.stealth.service.messages;

import com.backend.stealth.dto.request.messages.CreateQaRequest;
import com.backend.stealth.dto.request.messages.SendMessageRequest;
import org.springframework.http.ResponseEntity;

public interface MessageService {
    ResponseEntity<Object> getInboxUser();
    ResponseEntity<Object> sendMessage(SendMessageRequest request);
    ResponseEntity<Object> createTitleQAndDA(CreateQaRequest request);
    ResponseEntity<Object> showUserQuestion(String id, String username);
}
