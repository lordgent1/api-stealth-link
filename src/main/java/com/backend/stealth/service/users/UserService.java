package com.backend.stealth.service.users;

import org.springframework.http.ResponseEntity;

public interface UserService {
    ResponseEntity<Object> getProfile();
}
