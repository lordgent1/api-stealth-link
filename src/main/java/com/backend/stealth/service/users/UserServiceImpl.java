package com.backend.stealth.service.users;

import com.backend.stealth.dto.response.ResponseHandler;
import com.backend.stealth.dto.response.users.ShowProfileResponse;
import com.backend.stealth.entity.Users;
import com.backend.stealth.helper.UserAuth;
import com.backend.stealth.repository.MessageRepository;
import com.backend.stealth.repository.UsersRepository;
import org.modelmapper.ModelMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Service;

import java.util.Optional;

@Service
public class UserServiceImpl implements UserService {
    @Autowired
    UsersRepository usersRepository;
    @Autowired
    MessageRepository messageRepository;
    @Autowired
    UserAuth userAuth;

    @Override
    public ResponseEntity<Object> getProfile(){

        Optional<Users> user = usersRepository.findByUsername(userAuth.getUserLogin().getUsername());
        int totalSender = messageRepository.countMessage(userAuth.getUserLogin().getId());

        ModelMapper modelUser = new ModelMapper();

        ShowProfileResponse response = modelUser.map(user, ShowProfileResponse.class);
        response.setTotalSender(totalSender);

        return ResponseHandler.generateResponseSuccess(response);
    }


}

