package com.backend.stealth.service.auth;

import com.backend.stealth.dto.request.auth.SignInRequest;
import com.backend.stealth.dto.request.auth.SignUpRequest;
import org.springframework.http.ResponseEntity;

public interface AuthService {
    ResponseEntity<Object> signIn(SignInRequest request);
    ResponseEntity<Object> signUp(SignUpRequest request);
}
