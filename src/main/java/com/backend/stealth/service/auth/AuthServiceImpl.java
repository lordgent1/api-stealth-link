package com.backend.stealth.service.auth;

import com.backend.stealth.dto.request.auth.SignInRequest;
import com.backend.stealth.dto.request.auth.SignUpRequest;
import com.backend.stealth.dto.response.ResponseHandler;
import com.backend.stealth.dto.response.auth.SignInResponse;
import com.backend.stealth.entity.Users;
import com.backend.stealth.repository.UsersRepository;
import com.backend.stealth.security.jwt.JwtService;
import lombok.RequiredArgsConstructor;
import org.apache.catalina.User;
import org.modelmapper.ModelMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.security.authentication.AuthenticationManager;
import org.springframework.security.authentication.BadCredentialsException;
import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.stereotype.Service;

import java.util.UUID;


@Service
@RequiredArgsConstructor
public class AuthServiceImpl implements AuthService {


    private final ModelMapper modelMapper = new ModelMapper();

    @Autowired
    UsersRepository userRepository;
    @Autowired
    PasswordEncoder passwordEncoder;
    @Autowired
    private AuthenticationManager authenticationManager;
    @Autowired
    JwtService jwtService;

    @Override
    public ResponseEntity<Object> signIn(SignInRequest request){
        try{
            Authentication authentication = authenticationManager
                    .authenticate(new UsernamePasswordAuthenticationToken(request.getUsername(), request.getPassword()));

            SecurityContextHolder.getContext().setAuthentication(authentication);

            var data  = userRepository.findByUsername(request.getUsername()).orElseThrow();

            String token = jwtService.generateToken(data);

            SignInResponse response = new SignInResponse(token,data.getUsername());

            return ResponseHandler.generateResponseSuccess(response);
        }catch (BadCredentialsException e) {
            return ResponseHandler.generateResponseError(HttpStatus.UNAUTHORIZED, "Unauthorized", "Bad credentials");
        }
        catch (Exception e){
            return ResponseEntity.internalServerError().body(e.getMessage());
        }
    }

    @Override
    public ResponseEntity<Object> signUp(SignUpRequest request){

        boolean find = userRepository.existsByEmailOrUsername(request.getUsername(), request.getEmail());

        if(find){
            return ResponseHandler.generateResponseError(HttpStatus.BAD_REQUEST,"bad request","User Already Exist");
        }

        Users userMapper = modelMapper.map(request, Users.class);
        userMapper.setId(UUID.randomUUID().toString());
        userMapper.setPassword(passwordEncoder.encode(request.getPassword()));

        var data = userRepository.save(userMapper);
        return ResponseHandler.generateResponseSuccess(data);
    }


}
