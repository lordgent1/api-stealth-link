package com.backend.stealth.repository;

import com.backend.stealth.entity.Inbox;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public interface InboxRepository extends JpaRepository<Inbox,String> {
    @Query(value = "SELECT " +
            "i.id, u.username,i.title,u.thumbnail " +
            "FROM inbox i " +
            "INNER JOIN users u on i.user_id = u.id " +
            "WHERE i.id = :id AND u.username = :username",nativeQuery = true)
    List<Object[]> findQAndA(@Param("id") String id, @Param("username") String username);

    @Query(value = "SELECT i.id, i.title, u.username, COUNT(m.id) as totalSender " +
            "FROM inbox i " +
            "LEFT JOIN message m ON i.id = m.inbox_id " +
            "INNER JOIN users u ON i.user_id = u.id " +
            "WHERE u.username = :username " +
            "GROUP BY i.id, i.title, u.username " +
            "ORDER BY i.created_at DESC",nativeQuery = true)
    List<Object[]> listInboxUser( @Param("username") String username);

}
