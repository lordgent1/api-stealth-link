package com.backend.stealth.repository;

import com.backend.stealth.entity.Users;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

import java.util.Optional;

@Repository
public interface UsersRepository extends JpaRepository<Users,String> {
    Optional<Users> findByUsername(@Param("username") String username);

    @Query(value = "SELECT " +
            "CASE WHEN COUNT(u) > 0 " +
            "THEN true ELSE false END " +
            "FROM Users u " +
            "WHERE u.email = :email OR u.username = :username")
    boolean existsByEmailOrUsername(@Param("username") String username, @Param("email") String email);

}
