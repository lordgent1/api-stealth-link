package com.backend.stealth.repository;

import com.backend.stealth.entity.Message;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

@Repository
public interface MessageRepository extends JpaRepository<Message,String> {
    @Query(value = "SELECT COUNT(*) FROM message m INNER JOIN inbox i ON i.id = m.inbox_id where i.user_id = :userId",nativeQuery = true)
    int countMessage(@Param("userId") String userId);
}
